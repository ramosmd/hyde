![Badge Hyde](https://img.shields.io/badge/Hyde-0.8.9-green.svg)
![Badge Python](https://img.shields.io/badge/Python-2.7-yellow.svg)
![Badge Build](https://gitlab.com/themes-templates/hyde/badges/master/build.svg)

-----

View site: https://gitlab-examples.gitlab.io/pages-hyde/

-----


## Settings

- [Hyde] Default Theme + FontAwesome support
- Set up: `hyde create`
- Hyde config - view [`site.yaml`]
- GitLab CI - view [`.gitlab-ci.yml`]

### GitLab CI

What we need is to run the script on the environment of Python 2.7.x `image: python:2.7`, within a job called `pages` affecting only the `master` branch, install Hyde `pip install hyde` and generate our site to the `public` folder `hyde gen -d public`. We also add a `vendor` folder for cached pip modules and dependencies installation for speeding up our builds.

`.gitlab-ci.yml`:

<br>

```yaml
image: python:2.7

pages:
  cache:
    paths:
    - vendor/
  stage: deploy
  script:
    - pip install hyde
    - hyde gen -d public
  artifacts:
    paths:
    - public
  only:
    - master
```

### Testing builds in other branches

In order to test commits made to different branches, we just need a new job `test` affecting all branches except `master`:

<br>

```yaml
image: python:2.7

cache:
  paths:
  - vendor/

# the new job 'test'
test:
  stage: test
  script:
    - pip install hyde
    - hyde gen
  except:
    - master

pages:
  stage: deploy
  script:
    - pip install hyde
    - hyde gen -d public
  artifacts:
    paths:
    - public
  only:
    - master
```

### Testing and previewing the generated site

Besides adding a `test` job, we can also generate the site to a known folder, from which we can browse and download the site before merging to `master`. To do that, we need to set the `artifacts`  path after defining the output folder:

<br>

```yaml
# replace the job 'test' for this
test:
  stage: test # the stage is 'test'
  script:
    - pip install hyde
    - hyde gen -d test # generate the site to a 'test' folder
  artifacts:
    paths:
    - test # browse or download the artifacts for previewing locally - this will not override the folder 'public'
  except:
    - master
```

-----

## Usage

_**Note:** we assume you have [Hyde] installed, up and running locally._

- Fork, clone or download this project
- Configure Hexo [`site.yaml`] according to your project
- Navigate to the project folder (cd path/to/project)
- Run `hyde serve`

-----

Enjoy!

-----

[Hyde]: http://hyde.github.io/
[`site.yaml`]: https://gitlab.com/themes-templates/hyde/blob/master/site.yaml
[`.gitlab-ci.yml`]: https://gitlab.com/themes-templates/hyde/blob/master/.gitlab-ci.yml